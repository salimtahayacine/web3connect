import React,{useEffect, useState} from 'react'
import styles from '../styles/Home.module.css'
import { ethers } from 'ethers';
import {useWeb3React} from '@web3-react/core';
import {injected , WalletConnect, resetWalletConnector,walletlink, walletconnect} from '../Helpers/connectors';
import { Web3Provider } from '@ethersproject/providers';
import WalletLink from 'walletlink';
import WalletConnectProvider from '@walletconnect/ethereum-provider';
import { getAddress } from 'ethers/lib/utils';


const WalletCard = () => {
    //vanilaState
    const [ web3Library, setWeb3Library ] = React.useState();
	const [ web3Account, setWeb3Account ] = React.useState();
	const [ walletlinkProvider, setWalletlinkProvider ] = React.useState();
	const [ walletConnectProvider, setWalletConnectProvider ] = React.useState();
    ///////////////////

 const [showOption,setShowOption]= useState(false);
 const [errorMessage,setErrorMessage] = useState(null);
 const [defaultAccount,setDefaultAccount] = useState(null);
 const [userAddress,setUserAddress] = useState(null);
 const [userBalance,setUserBalance] = useState(null);
 //const [] = useState()

//connector, library, chainId, account, activate, deactivate
 const web3reactContext = useWeb3React();
//Simple connect metamask
const connectWalletHandler = () => {
    if(window.ethereum)
    { 
        // if the extension and the window of metamask is instaled
        window.ethereum.request({method: 'eth_requestAccounts'})
        .then(result => {
            accountChangeHandler(result[0]);
        })

    }else
    {
        setErrorMessage('Install Metamask');
    }
};
// disconect metamask using web3react
const disconectMetamaskSimple =() => {
    try{
        web3reactContext.deactivate();
    }catch (ex){
        console.log(ex)
        //alert(ex.errorMessage);
    }
};
//vanilla metamask
const connectMetamask = async () => {
    try {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        const account = accounts[0];
        //const setUserAddress = accounts[0];
        accountChangeHandler(accounts[0]);
        //onsole.log(account);
        setWeb3Account(account);
        const library = new Web3Provider(window.ethereum, 'any');
        //console.log('library');
        console.log(library);
        setWeb3Library(library);
    } catch (ex) {
        console.log(ex);
        //alert(ex.errorMessage);
    }
};
//vanilla walletconnect
const connectWaletConnect = async () => {
    try {
        const RPC_URLS = {
            1: 'https://mainnet.infura.io/v3/55d040fb60064deaa7acc8e320d99bd4',
            4: 'https://rinkeby.infura.io/v3/55d040fb60064deaa7acc8e320d99bd4'
        };
        const provider = new WalletConnectProvider({
            rpc: {
                1: RPC_URLS[1],
                4: RPC_URLS[4]
            },
            qrcode: true,
            pollingInterval: 15000
        });
        setWalletConnectProvider(provider);
        const accounts = await provider.enable();
        const account = accounts[0];
        accountChangeHandler(accounts[0]);

        const library = new Web3Provider(provider, 'any');

        console.log('library');
        console.log(library);
        setWeb3Library(library);
        setWeb3Account(account);
    } catch (ex) {
        console.log(ex);
    }
};
//vanilla coinbase
const connectCoinbase = async () => {
    try {
        // Initialize WalletLink
        const walletLink = new WalletLink({
            appName: 'demo-app',
            darkMode: true
        });
        
        const provider = walletLink.makeWeb3Provider(
            'https://rinkeby.infura.io/v3/55d040fb60064deaa7acc8e320d99bd4',
            4
        );
        setWalletlinkProvider(provider);
        const accounts = await provider.request({
            method: 'eth_requestAccounts'
        });
        const account = accounts[0];
        accountChangeHandler(accounts[0]);

        const library = new Web3Provider(provider, 'any');

        console.log('library');
        console.log(library);
        setWeb3Library(library);
        setWeb3Account(account);
    } catch (ex) {
        console.log(ex);
    }
};

//Account Change
const accountChangeHandler = (newAccount) => {
    setDefaultAccount(newAccount);
    getUserBalance(newAccount.toString());
    get
};
//ust balance
const [userUsdtBalance,setUserUsdtBalance]=useState(null);
// const usdtbalance = () =>{
//     window.ethereum.request({method:'usdt_getBalance',params:[Adress,'latest']})
//     .then(balance => {
//         setUserUsdtBalance(ethers.utils(balance));
//     })
// };
//get account balance in this method we call metamask
const getUserBalance = (Adress) => {
    window.ethereum.request({method: 'eth_getBalance',params : [Adress, 'latest']})
    .then(balance => {
        setUserBalance(ethers.utils.formatEther(balance));
        setUserUsdtBalance(ethers.ba);
    }) 
};

 useEffect(() => {
    window.ethereum.on('accountsChanged' , accountChangeHandler);
   // window.ethereum.on('chaineChanged', chaineChangedHandler );
//    getAddress('latest');
//    getUserBalance;
 },[showOption])
//  const chaineChangedHandler = () => {
//     window.location.reload();
//  }
const pickWalletHandler = () => {
    setShowOption(!showOption);
}
const disconnectCoinbase = () => {
     walletlinkProvider.close();
    //walletlinkProvider.disconnect()
    setWalletlinkProvider(null);
};
const disconnectWalletconnect = ()=>{
    walletConnectProvider.disconnect()
    setWalletConnectProvider(null);
}
//disconnectmetamsk 
const disconnectMetamaskSimple = () => {
    try {
        //web3reactContext.deactivate();
        web3Account.close();
        
    } catch (ex) {
        console.log(ex);
    }
};
// disconnect func 
const disconnectHandler =() =>
{
    disconectMetamaskSimple();
    alert('Check your Metamask Extension and disconnect')
    disconnectCoinbase();
    disconnectWalletconnect();
};

  return (
    <div className=''>
        <p className={styles.description}>
         {"Choose your wallet Bellow :"}
          <button onClick={pickWalletHandler}  className={styles.card}><h4 className=' text-blue-500 font-semibold'>Pick a wallet</h4></button>
          { showOption && 
          <div  className=' bg-slate-300 border-2 border-blue-300 rounded-md py-1 my-2 '>
            <ul className=' py-0'>
                <li className=' cursor-pointer hover:bg-blue-500 rounded-md hover:text-amber-500 ' onClick={connectMetamask}><div className=' flex items-center justify-center text-center'><img className='w-7 h-7 mx-2' src='./metamask2.svg'></img><h4 className=''>Metamask</h4></div></li>
                <li className=' cursor-pointer hover:bg-blue-500 rounded-md' onClick={connectWaletConnect}><div className=' flex items-center justify-center text-center'><img className='w-9 h-9 mx-2' src='./walletconnect.svg'></img><h4 className=''>WalletConnect</h4></div></li>
                <li className=' cursor-pointer hover:bg-blue-500 rounded-md' onClick={connectCoinbase}><div className=' flex items-center justify-center text-center'><img className='w-9 h-9 mx-' src='./coinbase.svg'></img><h4 className=''>CoinBase</h4></div></li>
            </ul>
          </div>}
          <div className='AccountDesplayInfos border-2 rounded-md p-4'>
            <div className=' flex justify-between'>
               <h4 className=' text-left'>Your Infos :</h4>
               {web3Account ? <button onClick={disconnectHandler} className=' border-2 rounded-md mx-2 px-2 bg-red-300'>Disconnect</button> : <div></div>}
            </div>
            <h3 className=' text-sm text-gray-600'>{'Note: If you want to truly disconnect from Metamask wallet from this page,'}<br/>{'disconnect from MetaMask extension.'}</h3>
            {web3Account ? <p className=' text-green-400'>Connected</p> : <p className=' text-red-500'>Not Connected </p>}
            {/* {web3Account ? <div>Connected By :</div> : <div></div>} */}
            <div className='AddressDesplay py-2'>
                <h3>Adress : {defaultAccount}</h3>
            </div>
            <div className='BalanceDesplay py-2'>
                <h3>Balance : {userBalance} ETH</h3>
                <h3> {userUsdtBalance} USDT</h3>
            </div>
            {errorMessage && <div>Erro: {errorMessage}</div>}
          </div>
        </p>
        <div className=' hidden'>
            {/* vanilla web3 */}
        <h2>Vanilla Controle</h2>
        {web3Account ? <p>{web3Account}</p> : <p>Not Connected</p>}
			{/* <div className=" flex space-x-3 ">
				<button
					className=" bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
					onClick={writeToContract}
				>
					Write to Contract
				</button>
			</div> */}
            {/* Metamask connection */}
            <div className="flex space-x-3">
				<button
					className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
					onClick={connectMetamask}
				>
					Connect Metamask
				</button>
			</div>
            <div className="flex space-x-3">
				<button
					className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
					onClick={connectWaletConnect}
				>
					Connect walletconnect
				</button>
				<button
					className=" bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded"
					onClick={disconnectWalletconnect}
				>
					Disconnect
				</button>
			</div>
            <div className="flex space-x-3">
				<button
					className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
					onClick={connectCoinbase}
				>
					Connect coinbase
				</button>
				<button
					className=" bg-pink-500 hover:bg-pink-700 text-white font-bold py-2 px-4 rounded"
					onClick={disconnectCoinbase}
				>
					Disconnect
				</button>
			</div>
        </div>
    </div>
  )
}

export default WalletCard